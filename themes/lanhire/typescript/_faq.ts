class question {

    _module: JQuery;
    _question: JQuery;
    _target: JQuery;
    _answer: JQuery;
    _icon: JQuery;

    constructor(module) {

        this._module = module;
        this._target = $('.question');
        this._question = $('.question');
        this._answer = $('.answer');
        this._icon = $('.fa-plus');
        this.addEventListener(event);
    }

    addEventListener(event) {

        this._target.on("click", (event) => this.handleQuestionToggle(event));

    }

    handleQuestionToggle(event) {

        const question = $(event.currentTarget);

        if (question.hasClass('is-shown')) return;

        this._question.removeClass('is-shown');

        question.addClass('is-shown');

    }
}

var toggleQuestion = new question($('.question'));
