var paymentOption = /** @class */ (function () {
    function paymentOption(module) {
        this._paymentselector = $('.payment-type');
        this._module = module;
        this.addEventListener();
    }
    paymentOption.prototype.addEventListener = function () {
        var _this = this;
        $(".open-select").on('click', function (event) { return _this.openSelectField(); });
        this._paymentselector.on('click', function (event) { return _this.selectPaymentMethod(event); });
    };
    paymentOption.prototype.openSelectField = function () {
        $(".payment-selector").toggleClass("open");
    };
    paymentOption.prototype.selectPaymentMethod = function (event) {
        var paymentMethod = $(event.currentTarget);
        if (paymentMethod.data("payment") == "ideal") {
            if (paymentMethod.hasClass('selected'))
                return;
            this._paymentselector.removeClass('selected');
            paymentMethod.addClass('selected');
            $("#idealBtn").removeClass('not-shown');
            $("#cash").addClass('not-shown');
            $('.buyform').attr("data-request", "onIdealCheckout");
            $(".payment-selector").removeClass("open");
            $(".disabled-type span").html("IDeal geselecteerd");
        }
        else if (paymentMethod.data("payment") == "cash") {
            if (paymentMethod.hasClass('selected'))
                return;
            this._paymentselector.removeClass('selected');
            paymentMethod.addClass('selected');
            $("#cash").removeClass('not-shown');
            $("#idealBtn").addClass('not-shown');
            $('.buyform').attr("data-request", "onCashCheckout");
            $(".payment-selector").removeClass("open");
            $(".disabled-type span").html("Contant betalen geselecteerd");
        }
    };
    return paymentOption;
}());
var payments = new paymentOption($(".payment-selector"));
//# sourceMappingURL=_payment.js.map