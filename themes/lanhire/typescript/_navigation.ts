class stickyHeader {

    _toggle: JQuery;
    _navigation: JQuery;
    constructor() {


        this._toggle = $('.menutoggle');
        this._navigation = $('.navigation');
        this.addEventListener();
    }

    addEventListener() {
        this._toggle.on('click', event => this.toggleNav());
        $(window).scroll((event) => this.windowScrollHandler());
    }

    windowScrollHandler() {
        const scrollTop = $(window).scrollTop();


        if (scrollTop > 200) {

            this._navigation.addClass('is-sticky');

        } else {

            this._navigation.removeClass('is-sticky');
        }
    }

    toggleNav() {
        this._navigation.toggleClass("active");
    }
}

const stickyheader = new stickyHeader();
