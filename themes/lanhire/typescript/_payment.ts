class paymentOption {

    _paymentselector: JQuery;
    _module: JQuery;
    constructor(module) {
        this._paymentselector = $('.payment-type');
        this._module = module;
        this.addEventListener();
    }

    addEventListener() {
        $(".open-select").on('click', event => this.openSelectField());
        this._paymentselector.on('click', event => this.selectPaymentMethod(event));
    }

    openSelectField() {
        $(".payment-selector").toggleClass("open");
    }

    selectPaymentMethod(event) {
        const paymentMethod = $(event.currentTarget);
        if (paymentMethod.data("payment") == "ideal") {

            if (paymentMethod.hasClass('selected')) return;

            this._paymentselector.removeClass('selected');
            paymentMethod.addClass('selected');

            $("#idealBtn").removeClass('not-shown');
            $("#cash").addClass('not-shown');

            $('.buyform').attr("data-request", "onIdealCheckout");

            $(".payment-selector").removeClass("open");
            $(".disabled-type span").html("IDeal geselecteerd");

        } else if (paymentMethod.data("payment") == "cash") {

            if (paymentMethod.hasClass('selected')) return;
            this._paymentselector.removeClass('selected');

            paymentMethod.addClass('selected');
            $("#cash").removeClass('not-shown');
            $("#idealBtn").addClass('not-shown');
            $('.buyform').attr("data-request", "onCashCheckout");
            $(".payment-selector").removeClass("open");
            $(".disabled-type span").html("Contant betalen geselecteerd");
        }

    }


}

const payments = new paymentOption($(".payment-selector"));
