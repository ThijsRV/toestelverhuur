class gameSelector {
    _module: JQuery;
    constructor(module) {
        this._module = module;
        this.addEventListener();
    }

    addEventListener() {
        $('input[type="checkbox"]').on('click', event => this.getCheckedValue(event));
    }

    getCheckedValue(event) {
        const games = [];

        $('input:checked').each(function () {
            games.push($(this).val());
        });

        const gamesString = JSON.stringify(games);

        const input = $('.games_field').val(gamesString);
    }
}

const selectGames = new gameSelector($('.game-form'));
