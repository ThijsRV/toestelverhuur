var question = /** @class */ (function () {
    function question(module) {
        this._module = module;
        this._target = $('.question');
        this._question = $('.question');
        this._answer = $('.answer');
        this._icon = $('.fa-plus');
        this.addEventListener(event);
    }
    question.prototype.addEventListener = function (event) {
        var _this = this;
        this._target.on("click", function (event) { return _this.handleQuestionToggle(event); });
    };
    question.prototype.handleQuestionToggle = function (event) {
        var question = $(event.currentTarget);
        if (question.hasClass('is-shown'))
            return;
        this._question.removeClass('is-shown');
        question.addClass('is-shown');
    };
    return question;
}());
var toggleQuestion = new question($('.question'));
//# sourceMappingURL=_faq.js.map