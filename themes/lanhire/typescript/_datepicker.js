var DatePicker = /** @class */ (function () {
    function DatePicker() {
        this._datepicker = $('.datepicker');
        this.addEventListener();
    }
    DatePicker.prototype.addEventListener = function () {
        var _this = this;
        $(window).on('load', function (event) { return _this.datepicker(); });
    };
    DatePicker.prototype.datepicker = function () {
        if (this._datepicker) {
            this._datepicker.datepicker($.datepicker.regional["nl"]);
            this._datepicker.datepicker({
                dateFormat: 'dd-mm-yy',
            });
        }
    };
    return DatePicker;
}());
var datepicker = new DatePicker();
//# sourceMappingURL=_datepicker.js.map