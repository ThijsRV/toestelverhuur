var imageSlide = /** @class */ (function () {
    function imageSlide() {
        this._sideImage = $(".side-image");
        this._mainIMage = $('.main-image');
        this.addEventListener();
    }
    imageSlide.prototype.addEventListener = function () {
        var _this = this;
        this._sideImage.on('click', function (event) { return _this.clickHandler(event); });
    };
    imageSlide.prototype.clickHandler = function (event) {
        var sideitem = $(event.currentTarget);
        this.sideImageBox(sideitem);
        this.changeMainImage(sideitem);
    };
    imageSlide.prototype.sideImageBox = function (sideitem) {
        if (sideitem.hasClass('active'))
            return;
        this._sideImage.removeClass('active');
        sideitem.addClass('active');
    };
    imageSlide.prototype.changeMainImage = function (sideitem) {
        this._mainIMage.children('img').attr('src', sideitem.children('img').attr('src'));
    };
    return imageSlide;
}());
var slide = new imageSlide();
//# sourceMappingURL=_gallery.js.map