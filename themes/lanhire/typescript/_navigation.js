var stickyHeader = /** @class */ (function () {
    function stickyHeader() {
        this._toggle = $('.menutoggle');
        this._navigation = $('.navigation');
        this.addEventListener();
    }
    stickyHeader.prototype.addEventListener = function () {
        var _this = this;
        this._toggle.on('click', function (event) { return _this.toggleNav(); });
        $(window).scroll(function (event) { return _this.windowScrollHandler(); });
    };
    stickyHeader.prototype.windowScrollHandler = function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 200) {
            this._navigation.addClass('is-sticky');
        }
        else {
            this._navigation.removeClass('is-sticky');
        }
    };
    stickyHeader.prototype.toggleNav = function () {
        this._navigation.toggleClass("active");
    };
    return stickyHeader;
}());
var stickyheader = new stickyHeader();
//# sourceMappingURL=_navigation.js.map