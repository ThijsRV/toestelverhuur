class DatePicker {

    public _datepicker: JQuery;

    constructor() {
        this._datepicker = $('.datepicker');
        this.addEventListener();
    }

    addEventListener() {
        $(window).on('load', event => this.datepicker());
    }

    datepicker() {
        if (this._datepicker) {
            this._datepicker.datepicker($.datepicker.regional["nl"]);
            this._datepicker.datepicker({
                dateFormat: 'dd-mm-yy',
            });
        }
    }
}

const datepicker = new DatePicker();
