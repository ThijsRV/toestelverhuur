class basket {

    public _baskettoggle: JQuery;
    public _basket: JQuery;
    constructor() {
        this._basket = $('.shopping-basket');
        this._baskettoggle = $('.basket-icon');
        this.addEventListener();
    }

    addEventListener() {
        this._basket.on('click', event => this.openBasket(event));
        $(window).on('click', event => this.closeBasket(event));
    }
    openBasket(event) {
        this._basket.addClass('open');
    }

    closeBasket(event) {
        if (!this._basket.is(event.target) &&
            !$('.cart-btn').is(event.target) &&
            this._basket.has(event.target).length === 0 &&
            $('.cart-btn').has(event.target).length === 0 ) {
            
            this._basket.removeClass('open');
        }
    }

}

const shoppingbasket = new basket();
