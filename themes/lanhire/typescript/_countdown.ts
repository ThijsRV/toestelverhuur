class countDownTimer {

    public discountdate: any;
    constructor() {
        $(document).ready(function () { 
            if ($(".stopwatch").length) {
                //TODO optimize this code
                this.discountdate = $('.discountdate').val();
                var countDownDate = new Date(this.discountdate).getTime();

                var x = setInterval(function () {


                    var now = new Date().getTime();

                    var distance = countDownDate - now;

                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);


                    document.getElementById("stopwatch").innerHTML =
                        "<span>" +
                        "<span>" + days + "d " + "</span>" +
                        "<span>" + hours + "u " + "</span>" +
                        "<span>" + minutes + "m " + "</span>" +
                        "<span>" + seconds + "s " + "</span>" +
                        "</span>";

                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById("stopwatch").innerHTML = "EXPIRED";
                    }
                }, 1000);
            }
        });
    }
}

const countdown = new countDownTimer();
