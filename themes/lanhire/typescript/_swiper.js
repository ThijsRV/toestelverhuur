var slider = /** @class */ (function () {
    function slider(module) {
        this._module = module;
        this.addEventListener(event);
    }
    slider.prototype.addEventListener = function (event) {
        var _this = this;
        $(window).on('load', function (event) { return _this.handleSlide(); });
    };
    slider.prototype.handleSlide = function () {
        var slider = new Swiper('.swiper-container', {
            direction: 'horizontal',
            loop: true,
            pagination: {
                el: '.swiper-pagination',
            },
            autoplay: {
                delay: 4000,
            },
        });
    };
    return slider;
}());
var Slidercontainer = new slider($('.swiper-container'));
//# sourceMappingURL=_swiper.js.map