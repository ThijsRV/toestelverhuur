var gameSelector = /** @class */ (function () {
    function gameSelector(module) {
        this._module = module;
        this.addEventListener();
    }
    gameSelector.prototype.addEventListener = function () {
        var _this = this;
        $('input[type="checkbox"]').on('click', function (event) { return _this.getCheckedValue(event); });
    };
    gameSelector.prototype.getCheckedValue = function (event) {
        var games = [];
        $('input:checked').each(function () {
            games.push($(this).val());
        });
        var gamesString = JSON.stringify(games);
        var input = $('.games_field').val(gamesString);
    };
    return gameSelector;
}());
var selectGames = new gameSelector($('.game-form'));
//# sourceMappingURL=_gameSelector.js.map