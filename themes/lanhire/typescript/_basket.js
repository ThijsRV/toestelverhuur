var basket = /** @class */ (function () {
    function basket() {
        this._basket = $('.shopping-basket');
        this._baskettoggle = $('.basket-icon');
        this.addEventListener();
    }
    basket.prototype.addEventListener = function () {
        var _this = this;
        this._basket.on('click', function (event) { return _this.openBasket(event); });
        $(window).on('click', function (event) { return _this.closeBasket(event); });
    };
    basket.prototype.openBasket = function (event) {
        this._basket.addClass('open');
    };
    basket.prototype.closeBasket = function (event) {
        if (!this._basket.is(event.target) &&
            !$('.cart-btn').is(event.target) &&
            this._basket.has(event.target).length === 0 &&
            $('.cart-btn').has(event.target).length === 0) {
            this._basket.removeClass('open');
        }
    };
    return basket;
}());
var shoppingbasket = new basket();
//# sourceMappingURL=_basket.js.map