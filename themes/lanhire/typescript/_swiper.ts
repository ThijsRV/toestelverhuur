class slider {
    _module: JQuery;
    constructor(module) {
        this._module = module;
        this.addEventListener(event);
    }

    addEventListener(event) {

        $(window).on('load', event => this.handleSlide());
    }

    handleSlide() {
        var slider = new Swiper('.swiper-container', {
            direction: 'horizontal',
            loop: true,

            pagination: {
                el: '.swiper-pagination',
            },

            autoplay: {
                delay: 4000,
            },
        });
    }
}

var Slidercontainer = new slider($('.swiper-container'));
