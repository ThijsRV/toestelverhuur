<?php namespace Thijsroelofse\Categories\Models;

use Model;

/**
 * Category Model
 */
class Category extends Model
{

    /**
     * @var array Generate slugs for these attributes.
     */
    /**
     * @var string The database table used by the model.
     */
    public $table = 'thijsroelofse_categories_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['categoryname'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => ['Thijsroelofse\products\Models\Product'],
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'categoryimage' => 'System\Models\File'
    ];
    public $attachMany = [];

}
