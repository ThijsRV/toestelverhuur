<?php namespace Thijsroelofse\Categories\Components;

use Cms\Classes\ComponentBase;
use Thijsroelofse\categories\Models\Category;

class Categories extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Categories Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->page['categories'] = $this->getAllCategories();
        $this->page['category']   = $this->getCategoryDetails();  
    }

    public function getAllCategories(){
        return Category::all();
    }

    public function getCategoryDetails(){
        $slug = $this->param('category');

        $category = Category::where('slug', '=', $slug)->first();

        return $category;
    }
}
