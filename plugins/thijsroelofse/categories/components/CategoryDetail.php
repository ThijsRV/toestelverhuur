<?php namespace Thijsroelofse\Categories\Components;

use Cms\Classes\ComponentBase;
use Thijsroelofse\products\Models\Product;
use Thijsroelofse\categories\Models\Category;
use Input;
use Response;
class CategoryDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Category Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->page["products"] = $this->getProductsInCategory();
    }

    public function getProductsInCategory(){

        $slug = $this->param('category');
        $category = Category::where('slug', $slug)->first();

        $products = Product::where("category_id", $category)->orderBy('price', 'DESC')->get();

        if($category == null){
            return $this->controller->run('404');
        }

        return $category->products;
    }
}
