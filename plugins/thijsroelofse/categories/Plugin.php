<?php namespace Thijsroelofse\Categories;

use Backend;
use System\Classes\PluginBase;

/**
 * categories Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'categories',
            'description' => 'No description provided yet...',
            'author'      => 'thijsroelofse',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Thijsroelofse\Categories\Components\Categories' => 'CategorieList',
            'Thijsroelofse\Categories\Components\CategoryDetail' => 'CategoryDetails',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'thijsroelofse.categories.some_permission' => [
                'tab' => 'categories',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

    return [];

        return [
            'categories' => [
                'label'       => 'Categories',
                'url'         => Backend::url('thijsroelofse/categories/categories'),
                'icon'        => 'icon-leaf',
                'permissions' => ['thijsroelofse.categories.*'],
                'order'       => 500,
            ],
        ];
    }
}
