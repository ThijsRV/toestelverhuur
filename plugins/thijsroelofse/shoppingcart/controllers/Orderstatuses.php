<?php namespace Thijsroelofse\Shoppingcart\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Orderstatuses Back-end Controller
 */
class Orderstatuses extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Thijsroelofse.Shoppingcart', 'shoppingcart', 'orderstatuses');
    }
}
