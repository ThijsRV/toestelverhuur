<?php namespace Thijsroelofse\Shoppingcart;

use Backend;
use System\Classes\PluginBase;

/**
 * shoppingcart Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'shoppingcart',
            'description' => 'No description provided yet...',
            'author'      => 'thijsroelofse',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Thijsroelofse\Shoppingcart\Components\ShoppingCart' => 'shoppingcart',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'thijsroelofse.shoppingcart.some_permission' => [
                'tab' => 'shoppingcart',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'shoppingcart' => [
                'label'       => 'Bestellingen',
                'url'         => Backend::url('thijsroelofse/shoppingcart/orders'),
                'icon'        => 'icon-shopping-basket',
                'permissions' => ['thijsroelofse.shoppingcart.*'],
                'order'       => 500,

                'sideMenu' => [
                    'newPage' => [
                        'label'       => 'Orderstatuses',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('thijsroelofse/shoppingcart/orderstatuses'),
                        'permissions' => ['thijsroelofse.shoppingcart.*'],
                    ],
                ]
            ]
        ];
    }
}
