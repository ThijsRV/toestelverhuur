<?php namespace Thijsroelofse\Shoppingcart\Models;

use Model;
use Thijsroelofse\Shoppingcart\Models\Cart;
/**
 * OrderStatus Model
 */
class OrderStatus extends Model
{
    use \October\Rain\Database\Traits\Nullable;
    protected $nullable = [];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'thijsroelofse_shoppingcart_orderstatus';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['status', 'trid'];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'orderstatus' => [
            'thijsroelofse\Shoppingcart\Models\Order',
            'order'     => 'created_at desc',
            'key'       => 'trid',
            'otherKey'  => 'trid'
        ],
    ];
}
