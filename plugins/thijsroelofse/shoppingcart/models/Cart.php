<?php namespace Thijsroelofse\Shoppingcart\Models;

use Model;
use Thijsroelofse\Products\Models\Product;
/**
 * Cart Model
 */
class Cart
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;

    public function __construct($oldBasket){
        if($oldBasket){
            $this->items = $oldBasket->items;
            $this->totalQty = $oldBasket->totalQty;
            $this->totalPrice = $oldBasket->totalPrice;
        }
    }
    public function add($item, $id){
        $storedItem = [ 'qty' => 0, 'salesprice' => $item->salesprice, 'item' => $item];

        if($this->items) {
            if(array_key_exists($id, $this->items)){
                $storedItem = $this->items[$id];
            }
        }
        $storedItem['qty']++;
        $storedItem['salesprice'] = $item->salesprice * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        $this->totalQty++;
        $this->totalPrice += $item->salesprice;
    }

    public function removeItem($item, $id){
        $this->totalQty -= $this->items[$id]['qty'];
        $this->totalPrice -= $this->items[$id]['salesprice'];
        unset($this->items[$id]);

    }


}
