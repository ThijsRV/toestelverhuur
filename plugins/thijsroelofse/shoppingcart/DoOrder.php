<?php

namespace thijsroelofse\shoppingcart;

use thijsroelofse\shoppingcart\Models\Cart;
use thijsroelofse\shoppingcart\Models\Order;
use thijsroelofse\products\Models\Product;
use thijsroelofse\shoppingcart\Models\OrderStatus;
use Validator;
use ValidationException;
use thijsroelofse\shoppingcart\Components\ShoppingCart;
use Session;
use Request;
use Mail;
use Redirect;
use Flash;
Class doOrder{

    private $flush;

    public function createOrderRequest($param){

        $oldBasket = Session::has('basket') ? Session::get('basket') : null;
        $basket = new Cart($oldBasket);

        $items = json_encode($basket->items);

        $hash = $this->hash();
        $amount = filter_var($basket->totalPrice + 3.95, FILTER_VALIDATE_FLOAT);


        $redirectUrl = str_replace(
            ['[uid]', '[uuid]', '[hash]'],
            Session::flush(),
            url('succes'),
            $hash
        );

        $data = post();

        $params = [
            'amount' => $amount,
            'description' => 'Product bestelling Toestelverhuur',
            'redirectUrl' => $redirectUrl,
            'webhookUrl' => route('webhook'),
            'metadata' => [
                'uid'=> $hash
            ]
        ];

        $rules = [
            'fullname' => 'required',
            'city'      => 'required',
            'email'     => 'required|email',
            'address'    => 'required',
            'zipcode'   => 'required',
            'phonenumber'   => 'required',
	        'policy'		=> 'required',
        ];


        $validation = Validator::make($data, $rules, self::$messages);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        // Request payment from Mollie
        $mollie = new \Mollie_API_Client;
        $mollie->setApiKey(env('MOLLIE_KEY'));

        $payment = $mollie->payments->create($params);

       if(!$basket->items){
            return Redirect::refresh();
       }

        OrderStatus::create([
            'trid' => $payment->id,
            'status' => 'open',
        ]);

        $order = new Order;

        $order->fullname = Request::Input('fullname');
        $order->email = Request::Input('email');
        $order->phonenumber = Request::Input('phonenumber');
        $order->address = Request::Input('address');
        $order->addition = Request::Input('addition');
        $order->city = Request::Input('city');
        $order->zipcode = Request::Input('zipcode');
        $order->order_extra = Request::Input('order_extra');
        $order->products = $items;
        $order->amount = $amount;
        $order->trid = $payment->id;
        $order->uuid = $hash;

        $order->save();

       $this->sendOrderToUser($data);
       $this->sendOrderToAdmin($data);
       return redirect()->to($payment->links->paymentUrl);
    }
   public function webhook(){
       $id = input('id');
       $mollie = new \Mollie_API_Client;
       $mollie->setApiKey(env('MOLLIE_KEY'));
       $payment = $mollie->payments->get($id);

       OrderStatus::create([
            'trid' => $payment->id,
            'status' => $payment->status,
       ]);
   }

   public function hash(){

        return bin2hex(random_bytes(16));
   }

   public function sendOrderToAdmin($data){
       Mail::send('order.buy.admin.mail', $data, function($message) use ($data){

            $message->to('info@toestelverhuur.nl', 'Thijs Roelofse', $data);
            $message->subject('Nieuwe aankoop');

        });
   }

   public function sendOrderToUser($data){
         Mail::send('order.buy.user.email', $data, function($message) use ($data) {

            $message->to($data['email'], $data['fullname']);
            $message->subject('Uw bestelling is geplaatst');

        });
    }

   public static $messages = array(
        'policy.required' => 'U dient akkoord te gaan',
        '*.required' => 'Dit veld is verplicht',
        'email.email' => 'Dit emailadres is niet geldig',
   );
}
