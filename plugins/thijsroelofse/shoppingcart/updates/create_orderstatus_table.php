<?php namespace Thijsroelofse\Shoppingcart\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrderStatusTable extends Migration
{
    public function up()
    {
        Schema::create('thijsroelofse_shoppingcart_orderstatus', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('trid');
            $table->string('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('thijsroelofse_shoppingcart_orderstatus');
    }
}
