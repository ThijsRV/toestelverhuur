<?php namespace Thijsroelofse\Shoppingcart\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('thijsroelofse_shoppingcart_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('fullname');
            $table->string('email');
            $table->string('phonenumber')->nullable();
            $table->string('address');
            $table->string('addition')->nullable();
            $table->string('city');
            $table->string('zipcode');
            $table->text('order_extra')->nullable();
            $table->json('products')->nullable();
            $table->string('amount');
            $table->string('uuid');
            $table->string('trid');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('thijsroelofse_shoppingcart_orders');
    }
}
