<?php namespace Thijsroelofse\Shoppingcart\Components;

use Cms\Classes\ComponentBase;
use ThijsRoelofse\ShoppingCart\Models\Cart;
use ThijsRoelofse\ShoppingCart\Models\Order;
use ThijsRoelofse\ShoppingCart\Models\OrderStatus;
use ThijsRoelofse\Products\Models\Game;
use ThijsRoelofse\Products\Models\Product;
use Session;
use ValidationException;
use Validator;
use Input;
use Mail;
use Redirect;
use Request;
class ShoppingCart extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ShoppingCart Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function emptyBasket(){
        return [
            
            '.basket-page' => $this->renderPartial('basket/emptybasketpage')
        ];
    }


    //Add
    public function onAddToBasket(){

        $input = Input::get('addProduct');
        $product = Product::find($input);

        $oldBasket = Session::has('basket') ? Session::get('basket') : null;
        $basket = new Cart($oldBasket);

        $basket->add($product, $product->id);

        $basket = Session::put('basket', $basket);
    }


    public static function getBasketItems(){

        $oldBasket = Session::get('basket');
        $basket = new Cart($oldBasket);

        return $basket->items;

    }

    public function getBasketPrice(){

        $oldBasket = Session::get('basket');
        $basket = new Cart($oldBasket);

        return $basket->totalPrice;
    }

    public function getBasketQty(){

        $oldBasket = Session::has('basket') ? Session::get('basket') : null;
        $basket = new Cart($oldBasket);

        return $basket->totalQty;
        
    }
    //Remove

    public function onRemoveItem(){
        $input = Input::get('removeproduct');
        $product = Product::find($input);

        $oldBasket = Session::has('basket') ? Session::get('basket') : null;
        $basket = new Cart($oldBasket);

        $basket->removeItem($product, $product->id);
        $basket = Session::put('basket', $basket);

   }

   public function onIdealCheckOut(){
        $checkout = new \thijsroelofse\shoppingcart\doOrder();

        return $checkout->createOrderRequest(request()->all());
   }
   
    public function onCashCheckout(){

        $oldBasket = Session::has('basket') ? Session::get('basket') : null;
        $basket = new Cart($oldBasket);

        $items = json_encode($basket->items);
        $amount = filter_var($basket->totalPrice + 3.95, FILTER_VALIDATE_FLOAT);

        $data = post();

        $rules = [
            'fullname' => 'required',
            'city'      => 'required',
            'email'     => 'required|email',
            'address'    => 'required',
            'zipcode'   => 'required',
	        'policy'		=> 'required',
        ];


        $validation = Validator::make($data, $rules, self::$messages);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }
        if(!$basket->items){
            return Redirect::refresh();
        }

        $order = new Order;
        $order->fullname = Request::Input('fullname');
        $order->email = Request::Input('email');
        $order->address = Request::Input('address');
        $order->addition = Request::Input('addition');
        $order->city = Request::Input('city');
        $order->zipcode = Request::Input('zipcode');
        $order->order_extra = Request::Input('order_extra');
        $order->products = $items;
        $order->amount = $amount;
        $order->trid = "contant";
        $order->uuid = "contant";

        $order->save();

        //$this->sendOrderToAdmin($data);
        //$this->sendOrderToUser($data);

        Session::flush();

        return redirect('succes');
    }

    public static $messages = array(
        'policy.required' => 'U dient akkoord te gaan',
        '*.required' => 'Dit veld is verplicht',
        'email.email' => 'Dit emailadres is niet geldig',
   );

   

    public function sendOrderToAdmin($data){
       Mail::send('order.buy.admin.mail', $data, function($message) use ($data) {

            $message->to('info@toestelverhuur.nl', 'Thijs Roelofse', $data);
            $message->subject('Nieuwe aankoop');

        });
   }

   public function sendOrderToUser($data){
         Mail::send('order.buy.user.email', $data, function($message) use ($data) {

            $message->to($data['email'], $data['fullname']);
            $message->subject('Uw bestelling is geplaatst');

        });
    }
}
