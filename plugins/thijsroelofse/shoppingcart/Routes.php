<?php
/*
Routes specifiek voor deze plugin
 */

Route::name('webhook')->post(
        '/webhook',
        'thijsroelofse\shoppingcart\doOrder@webhook');

Route::post('/checkout', function () {

    $checkout = new thijsroelofse\shoppingcart\doOrder();

    return $checkout->createOrderRequest(request()->all());

});
