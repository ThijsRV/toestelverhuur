<?php namespace Thijsroelofse\Contactform;

use Backend;
use System\Classes\PluginBase;

/**
 * Contactform Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Contactform',
            'description' => 'No description provided yet...',
            'author'      => 'thijsroelofse',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Thijsroelofse\Contactform\Components\Contactform' => 'contactform',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'thijsroelofse.contactform.some_permission' => [
                'tab' => 'Contactform',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'contactform' => [
                'label'       => 'Contact aanvragen',
                'url'         => Backend::url('thijsroelofse/contactform/contacts'),
                'icon'        => 'icon-question-circle',
                'permissions' => ['thijsroelofse.contactform.*'],
                'order'       => 500,
            ],
        ];
    }
}
