<?php namespace Thijsroelofse\Contactform\Components;

use Cms\Classes\ComponentBase;
use Thijsroelofse\Contactform\Models\Contact;
use Validator;
use Mail;
use ValidationException;
class Contactform extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'contactform Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onContactSubmit(){
        $data = post();

       $rules = [
            'fullname' => 'required',
            'city'      => 'required',
            'email'     => 'required|email',
            'address'    => 'required',
            'zipcode'   => 'required',
	        'policy'		=> 'required',
        ];


        $messages = [
            '*.required' => 'Dit veld is verplicht',
            'email.email' => 'Dit emailadres is niet geldig',
        ];

        
        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {

            throw new ValidationException($validation);
        }

        Contact::create($data);
        $this->sendContactToAdmin($data);
          return [
            '.contact-form' => $this->renderPartial('forms/states/contactsuccess')
        ];
    }

    public function sendContactToAdmin($data){
        Mail::send('contact.admin.mail', $data, function($message) use ($data) {

        $message->to('info@toestelverhuur.nl', 'Thijs Roelofse', $data);
        $message->subject('Nieuwe Contact aanvraag');

        });
    }
}
