<?php namespace Thijsroelofse\Reparations;

use Backend;
use System\Classes\PluginBase;

/**
 * reparations Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'reparations',
            'description' => 'No description provided yet...',
            'author'      => 'thijsroelofse',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Thijsroelofse\Reparations\Components\Repairs' => 'Repairs',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'thijsroelofse.reparations.some_permission' => [
                'tab' => 'reparations',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'reparations' => [
                'label'       => 'Reparatie aanvragen',
                'url'         => Backend::url('thijsroelofse/reparations/reparations'),
                'icon'        => 'icon-wrench',
                'permissions' => ['thijsroelofse.reparations.*'],
                'order'       => 500,
            ],
        ];
    }
}
