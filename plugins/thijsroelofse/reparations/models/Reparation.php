<?php namespace Thijsroelofse\Reparations\Models;

use Model;

/**
 * reparation Model
 */
class Reparation extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'thijsroelofse_reparations_reparations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['fullname', 'email', 'phonenumber', 'city', 'zipcode', 'address', 'addition', 'select', 'description'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
