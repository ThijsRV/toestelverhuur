<?php namespace Thijsroelofse\Reparations\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Reparations Back-end Controller
 */
class Reparations extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Thijsroelofse.Reparations', 'reparations', 'reparations');
    }
}
