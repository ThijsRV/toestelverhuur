<?php namespace Thijsroelofse\Reparations\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateReparationsTable extends Migration
{
    public function up()
    {
        Schema::create('thijsroelofse_reparations_reparations', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('fullname');
            $table->string('email');
            $table->string('phonenumber')->nullable();
            $table->string('address');
            $table->string('addition')->nullable();
            $table->string('city');
            $table->string('zipcode');
            $table->string('select');
            $table->text('description');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('thijsroelofse_reparations_reparations');
    }
}
