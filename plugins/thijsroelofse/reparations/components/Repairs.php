<?php namespace Thijsroelofse\Reparations\Components;

use Cms\Classes\ComponentBase;
use ValidationException;
use Validator;
use thijsroelofse\Reparations\Models\Reparation;
use Mail;
class Repairs extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'repairs Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRepairSubmit(){
        $data = post();

        $rules = [
            'fullname' => 'required',
            'city'      => 'required',
            'email'     => 'required|email',
            'address'    => 'required',
            'zipcode'   => 'required',
            'select'    => 'required',
	        'policy'		=> 'required',
            'description'   => 'required',
        ];

        $messages = [
            '*.required' => 'Dit veld is verplicht',
            'email.email' => 'Dit emailadres is niet geldig',
        ];

        
        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {

            throw new ValidationException($validation);
        }

        Reparation::create($data);
        $this->sendContactToAdmin($data);
          return [
            '.reparations .container' => $this->renderPartial('forms/states/repairsuccess')
        ];
    }

    public function sendContactToAdmin($data){
        Mail::send('repair.admin.email', $data, function($message) use ($data) {

        $message->to('info@toestelverhuur.nl', 'Thijs Roelofse', $data);
        $message->subject('Nieuwe reparatie aanvraag');

        });
    }
}
