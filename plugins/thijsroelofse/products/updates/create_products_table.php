<?php namespace Thijsroelofse\Products\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('thijsroelofse_products_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string("productname");
            $table->text("description");
            $table->string("price")->nullable();
            $table->string("salesprice")->nullable();
            $table->string("bail")->nullable();
            $table->text("slug")->nullable();
            $table->text("additional_information")->nullable();
            $table->boolean('is_game');
            $table->boolean('is_device');
            $table->boolean("most_watched");
            $table->boolean("for_sale");
            $table->boolean("for_rent");
            $table->boolean("is_important");
            $table->integer("quantity");
            $table->integer("gamecategory_id")->nullable();
            $table->integer("category_id")->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('thijsroelofse_products_products');
    }
}
