<?php namespace Thijsroelofse\Products\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGamesCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('thijsroelofse_products_games_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('gamecategoryname');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('thijsroelofse_products_games_categories');
    }
}
