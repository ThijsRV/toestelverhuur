<?php namespace Thijsroelofse\Products\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsplatformsTable extends Migration
{
    public function up()
    {
        Schema::create('thijsroelofse_products_gameplatforms', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer("product_id");
            $table->integer("gameplatform_id");
            $table->primary(['product_id','gameplatform_id'], 'gameplatforms');
        });
    }

    public function down()
    {
        Schema::dropIfExists('thijsroelofse_products_gameplatforms');
    }
}
