<?php namespace Thijsroelofse\Products\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsGamesTable extends Migration
{
    public function up()
    {
        Schema::create('thijsroelofse_products_game_product', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer("product_id");
            $table->integer("game_id");
            $table->primary(['product_id','game_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('thijsroelofse_products_game_product');
    }
}
