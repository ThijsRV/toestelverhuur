<?php namespace Thijsroelofse\Products\Models;

use Model;

/**
 * GameCategory Model
 */
class GameCategory extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'thijsroelofse_products_games_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
