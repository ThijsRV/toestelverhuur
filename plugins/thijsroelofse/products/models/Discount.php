<?php namespace Thijsroelofse\Products\Models;

use Model;
/**
 * Discount Model
 */
class Discount extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $implement = ['System.Behaviors.SettingsModel'];
    
    public $settingsCode = 'thijsroelofse_discount_settings';

    public $settingsFields = 'fields.yaml';

       protected $cache = [];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['global_discount'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function setGlobalDiscount(){
        $discount = Discount::get('global_discount');

        if($discount === "1"){
            return true;
        }
        return false;
    }

    public static function setDiscountDate(){

        $discountDate =  Discount::get('discount_from');

        return $discountDate;
    }
}
