<?php namespace Thijsroelofse\Products\Models;

use Model;
use Thijsroelofse\Categories\Models\Category;
use Thijsroelofse\shoppingcart\Models\Order;


class Product extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'thijsroelofse_products_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
  protected $fillable = ['categoryname','id'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
             'orders' => ['Thijsroelofse\Hire\Models\Order'],  
    ];
    public $belongsTo = [
        'category' => ['Thijsroelofse\categories\Models\Category'],
        'gamecategory' => [
            'Thijsroelofse\products\Models\GameCategory',
            'table' => 'thijsroelofse_products_games_categories',
        ],

        'rents' => [ 'Thijsroelofse\Hire\Models\Order'],
    ];
    public $belongsToMany = [
        //'orders' => ['Thijsroelofse\shoppingcart\Models\Order'], 
        'gameplatforms' => [
            'Thijsroelofse\products\Models\Gameplatform',
            'table' => 'thijsroelofse_products_gameplatforms',
            'pivot' => ['product_id', 'gameplatform_id'],
        ],
        'Games' => [
            'Thijsroelofse\products\Models\Game',
            'table' => 'thijsroelofse_products_game_product',
        ],

    ];

    public $attachMany = [
            'productimages' => 'System\Models\File'
    ];

      public $attachOne = [
            'seo_image' => 'System\Models\File'
    ];
}
