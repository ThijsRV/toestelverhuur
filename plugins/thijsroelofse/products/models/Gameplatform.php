<?php namespace Thijsroelofse\Products\Models;

use Model;

/**
 * gameplatform Model
 */
class Gameplatform extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'thijsroelofse_gameplatforms';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
           'products' => [
            'Thijsroelofse\products\Models\Product',
            'table' => 'thijsroelofse_products_gameplatforms',
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
