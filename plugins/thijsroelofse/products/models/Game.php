<?php namespace Thijsroelofse\Products\Models;

use Model;

/**
 * Game Model
 */
class Game extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'thijsroelofse_products_games';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [

    ];
    public $hasMany = [
    ];
    public $belongsTo = [
        'gamescategory' => 'Thijsroelofse\Products\Models\GameCategory',
    ];
    public $belongsToMany = [
        'products' => [
            'Thijsroelofse\products\Models\Product',
            'table' => 'thijsroelofse_products_game_product',
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        "game_image" => "System\Models\File",
    ];
    public $attachMany = [
    ];
}
