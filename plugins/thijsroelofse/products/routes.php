<?php
    use Thijsroelofse\Products\Models\Product;
    use Thijsroelofse\Categories\Models\Category;

Route::get('sitemap.xml', function(){
    $products = Product::all();
    $categories = Category::all();

    return Response::view('thijsroelofse.products::sitemap',
          [
            'products' => $products,
            'categories' => $categories
          ])->header('Content-Type', 'text/xml');
});
