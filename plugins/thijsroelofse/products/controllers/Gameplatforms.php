<?php namespace Thijsroelofse\Products\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Gameplatforms Back-end Controller
 */
class Gameplatforms extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Thijsroelofse.Products', 'products', 'gameplatforms');
    }
}
