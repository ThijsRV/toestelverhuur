<?php namespace Thijsroelofse\Products;

use Backend;
use System\Classes\PluginBase;
use  System\Classes\SettingsManager;
/**
 * Products Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Products',
            'description' => 'No description provided yet...',
            'author'      => 'thijsroelofse',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Thijsroelofse\Products\Components\ProductItems' => 'ProductItems',
            'Thijsroelofse\Products\Components\ProductItem' => 'ProductItem',
            'Thijsroelofse\Products\Components\Discounts' => 'discounts',
            'Thijsroelofse\Products\Components\Gameplatforms' => 'gameplatforms',
            'Thijsroelofse\Products\Components\Games' => 'Games',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'thijsroelofse.products.some_permission' => [
                'tab' => 'Products',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'products' => [
                'label'       => 'Products',
                'url'         => Backend::url('thijsroelofse/products/products'),
                'icon'        => 'icon-leaf',
                'permissions' => ['thijsroelofse.products.*'],
                'order'       => 500,

                   'sideMenu' => [
                   'newPage' => [
                        'label'       => 'Categorie',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('thijsroelofse/categories/Categories'),
                        'permissions' => ['thijsroelofse.products.*'],
                    ],

                    'newPage2' => [
                        'label'       => 'Games',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('thijsroelofse/products/Games'),
                        'permissions' => ['thijsroelofse.products.*'],
                    ],
                    'newPage3' => [
                        'label'       => 'Game Genres',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('thijsroelofse/products/gamecategories'),
                        'permissions' => ['thijsroelofse.products.*'],
                    ],
                    'newPage4' => [
                        'label'       => 'Game Platforms',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('thijsroelofse/products/gameplatforms'),
                        'permissions' => ['thijsroelofse.products.*'],
                    ],
                ]
            ]
        ];
    }

public function registerSettings()
{
       return [
            'settings' => [
                'label' => 'Kortingen',
                'description' => 'Acties beheren',
                'icon' => 'icon-pencil-square-o',
                'class' => 'ThijsRoelofse\Products\Models\Discount',
                'category' => SettingsManager::CATEGORY_CMS
            ]
        ];
}
}

