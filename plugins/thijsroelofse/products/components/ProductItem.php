<?php namespace Thijsroelofse\Products\Components;

use Cms\Classes\ComponentBase;
use Thijsroelofse\Products\Models\Product;
use Thijsroelofse\Categories\Models\Category;
use View;
use Response;
class ProductItem extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ProductItem Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->page["product"] = $this->getProductDetails();
        $this->page['productgames'] = $this->getGamesInProduct();
    }

    public function getProductDetails(){
        $slug = $this->param('product');

        $product = Product::where('slug', '=', $slug)->first();

        if(!$product){
            return $this->controller->run('404');
        }

        return $product;
    }

    public function getGamesInProduct(){
        $slug = $this->param('product');

        $product = Product::where('slug', '=', $slug)->first();

        if(!$product){
            return $this->controller->run('404');
        }

        $games = $product->Games()->orderby('gamename', 'asc')->get();

        return $games;
    }
}
