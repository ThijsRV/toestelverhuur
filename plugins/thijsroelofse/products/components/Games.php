<?php namespace Thijsroelofse\Products\Components;
use Thijsroelofse\Products\Models\Game;
use Thijsroelofse\Products\Models\GameCategory;
use Thijsroelofse\Products\Models\Product;
use Cms\Classes\ComponentBase;
use Input;
use Request;
class Games extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Games Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->page['games'] = $this->getGames();
        $this->page['game_genres'] = $this->getGameCategories();
    }

    public function getGames(){
        $is_game = 1;

        if($is_game){
            $games = Product::where('is_game', $is_game)->OrderBy('productname', 'DESC')->get();

             return $games;
        }
    }

    public function getGameCategories(){
        $gamegenres = GameCategory::all();

        return $gamegenres;
    }

    public function onGameSearch(){
        $games = Input::get('game_search');
        if($games){
            $games = Product::where('is_game', 1)
                    ->where('productname', 'LIKE', '%'.$games.'%')->Orderby('productname', 'DESC')->get();

            $this->page['games'] = $games;
        }

        if(!$games){
            $this->page['games'] = $this->getGames();
        }
    }

    public function onGetGameForPlatform(){
        $games = Input::get('platforms');

        $checkbox = [];

        $checkbox[] = $games;

        if($games){
            $products = Product::whereHas('gameplatforms', function ($query) use ($games){
                $query->where('is_game', 1)->whereIn('gameplatform_id', $games);
            })->OrderBy('productname', 'DESC')->get();

            $this->page['games'] = $products;
        }
        if(!$games){
            $this->page['games'] = $this->getGames();
        }
    }

    public function onGetGameCategory(){
        $games = Input::get('gamecategories');
        $platform = Input::get('platforms');

        $gamescategory = [];

        $gamescategory[] = $games;

        if($games){
            $products = Product::where('is_game', 1)
                       ->whereIn('gamecategory_id', $games)
                       ->OrderBy('salesprice', 'DESC')->get();
        
            $this->page['games'] = $products;
        }else{
            $this->page['games'] = $this->getGames();
        }
    }
}
