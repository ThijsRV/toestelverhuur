<?php namespace Thijsroelofse\Products\Components;

use Cms\Classes\ComponentBase;
use Thijsroelofse\Products\Models\Product;
use Thijsroelofse\Products\Models\Discount;
class Discounts extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'discounts Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->page['globaldiscount'] = Discount::setGlobalDiscount();
        $this->page['discountdate'] = Discount::setdiscountdate();
    }
}
