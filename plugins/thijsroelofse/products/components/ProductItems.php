<?php namespace Thijsroelofse\Products\Components;

use Cms\Classes\ComponentBase;
use Thijsroelofse\Products\Models\Product;
use Thijsroelofse\Products\Models\Discount;
use Thijsroelofse\Categories\Models\Category;
class ProductItems extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ProductItems Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->page['hotproducts'] = $this->getHotProducts();
        $this->page['productsforsale'] = $this->getProductsForSale();
    }
    public function getHotProducts(){              
        $most_viewed = 1;
        if($most_viewed){
            $mostViewedProduct = Product::where('most_watched', $most_viewed)->get();
        }

        return $mostViewedProduct;
    }

    public function getProductsForSale(){
        $for_sale = 1;

        if($for_sale){
            $products = Product::where('for_sale', $for_sale)->get();
        }

        return $products;
                
    }

    public function getImportantProducts($limit = null, $offset = null){
            
        $products = Product::offset($offset)
                            ->limit($limit)
                            ->where('is_important', 1)
                            ->where('for_sale', 1)
                            ->where('quantity', '>', 0)
                            ->orderBy('id', 'DESC')
                            ->get();

        return $products;
    }

    public function getMainProduct(){
       return $this->getImportantProducts(1, 0);
    }

    public function getSideProducts(){
       return $this->getImportantProducts(2, 1);
    }

}
