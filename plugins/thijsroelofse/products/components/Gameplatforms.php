<?php namespace Thijsroelofse\Products\Components;

use Cms\Classes\ComponentBase;
use Thijsroelofse\Products\Models\Gameplatform;
class GamePlatforms extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'gameplatforms Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->page['platforms'] = $this->getGamePlatforms();
    }

    public function getGamePlatforms(){
        return Gameplatform::get();
    }
}
