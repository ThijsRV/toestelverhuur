<?php namespace Thijsroelofse\Users;

use Backend;
use System\Classes\PluginBase;

/**
 * users Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'users',
            'description' => 'No description provided yet...',
            'author'      => 'thijsroelofse',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Thijsroelofse\Users\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'thijsroelofse.users.some_permission' => [
                'tab' => 'users',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'users' => [
                'label'       => 'users',
                'url'         => Backend::url('thijsroelofse/users/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['thijsroelofse.users.*'],
                'order'       => 500,
            ],
        ];
    }
}
