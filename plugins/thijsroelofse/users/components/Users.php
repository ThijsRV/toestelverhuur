<?php namespace Thijsroelofse\Users\Components;

use Cms\Classes\ComponentBase;

class Users extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Users Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
