<?php namespace Thijsroelofse\Team;

use Backend;
use System\Classes\PluginBase;

/**
 * team Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'team',
            'description' => 'No description provided yet...',
            'author'      => 'thijsroelofse',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Thijsroelofse\Team\Components\Teammembers' => 'TeamMembers',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'thijsroelofse.team.some_permission' => [
                'tab' => 'team',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'team' => [
                'label'       => 'Team',
                'url'         => Backend::url('thijsroelofse/team/team'),
                'icon'        => 'icon-user',
                'permissions' => ['thijsroelofse.team.*'],
                'order'       => 500,
            ],
        ];
    }
}
