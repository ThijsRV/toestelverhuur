<?php namespace Thijsroelofse\Team\Components;

use Cms\Classes\ComponentBase;
use Thijsroelofse\team\Models\Team;
class Teammembers extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Teammembers Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getMembers(){
        return Team::all();
    }
}
