<?php namespace ThijsRoelofse\Hire\Models;

use Model;

/**
 * Order Model
 */
class Order extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'thijsroelofse_hire_orders';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable =[
        'fullname',
        'email',
        'city',
        'phonenumber',
        'address',
        'delivery_date',
        'pickup_date',
        'zipcode',
        'addition',
        'product_id',
        'productname',
        'price',
        'bail',
        'product_games',
        ];

    public $hasOne = [
       'product' => ['Thijsroelofse\products\Models\Product'], 
    ];
    public $hasMany = [
       'sales' => ['Thijsroelofse\Shoppingcart\Models\Order'],  
    ];
}
