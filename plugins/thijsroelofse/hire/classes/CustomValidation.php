<?php namespace Thijsroelofse\Hire\classes;

use Illuminate\Contracts\Validation\Rule;
use Request;
use Input;
use Carbon\Carbon;
class CustomValidation Implements Rule{

    public $message = 'Eind datum is kleiner dan begin datum';

    public function passes($attribute, $value){

        $attribute = Request::Input('minvalue');

        $minAttr = strtotime($attribute);
        $maxAttr = strtotime($value);


        if($maxAttr > $minAttr){
            return true;
        }elseif($value == ""){
            return false;
        }else{
            return false;
        }
    }

    public function message(){
        
        return $this->message;
    }
}
