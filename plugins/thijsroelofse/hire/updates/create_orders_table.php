<?php namespace ThijsRoelofse\Hire\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('thijsroelofse_hire_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('fullname');
            $table->string('email');
            $table->string('phonenumber')->nullable();
            $table->string('city');
            $table->string('address');
            $table->string('addition')->nullable();
            $table->string('delivery_date');
            $table->string('pickup_date');
            $table->text('order_extra')->nullable();
            $table->integer('product_id')->unsigned();
            $table->string('productname');
            $table->string('price');
            $table->string('bail');
            $table->longText('product_games')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('thijsroelofse_hire_orders');
    }
}
