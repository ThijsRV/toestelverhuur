<?php namespace Thijsroelofse\Hire;

use Backend;
use System\Classes\PluginBase;

/**
 * hire Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'hire',
            'description' => 'No description provided yet...',
            'author'      => 'thijsroelofse',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Thijsroelofse\Hire\Components\HireProduct' => 'HireProduct',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'thijsroelofse.hire.some_permission' => [
                'tab' => 'hire',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'orders' => [
                'label'       => 'Huuraanvragen',
                'url'         => Backend::url('thijsroelofse/hire/orders'),
                'icon'        => 'icon-money',
                'permissions' => ['thijsroelofse.products.*'],
                'order'       => 500,
            ],
        ];
    }
}
