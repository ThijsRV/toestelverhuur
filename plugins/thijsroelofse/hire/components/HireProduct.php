<?php namespace Thijsroelofse\Hire\Components;

use Cms\Classes\ComponentBase;
use Thijsroelofse\Hire\Models\Order;
use Thijsroelofse\Products\Models\Product;
use Thijsroelofse\Products\Models\Discount;
use Thijsroelofse\Categories\Models\Category;
use Request;
use Validator;
use ValidationException;
use Input;
use Mail;
use Carbon\Carbon;
use Thijsroelofse\Hire\classes\CustomValidation;
class HireProduct extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'HireProduct Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

	public function onOrderProduct()
	{
        $slug = $this->param('product');

        $product = Product::where('slug', $slug)->first();

        $data = post();

        $rules = [
            'fullname' => 'required',
            'city'      => 'required',
            'email'     => 'required|email',
            'phonenumber'   => 'required',
            'address'    => 'required',
            'minvalue'   => 'required|after:now',
            'maxvalue'   => [
                        new CustomValidation,
                        'required:after:now'
            ],
		    'policy'		=> 'required',
        ];
        $validation = Validator::make($data, $rules, self::$messages);

        if ($validation->fails()) {

            throw new ValidationException($validation);
        }

        if(Discount::setGlobalDiscount()){
            $price = $product->price - $product->price * 20 / 100;
        }else{
            $price = $product->price;
        }


        $data = [
            'fullname' => Request::Input('fullname'),
            'email' => Request::Input('email'),
            'phonenumber' => Request::Input('phonenumber'),
            'city' => Request::Input('city'),
            'address' => Request::Input('address'),
            'addition' => Request::Input('addition'), 
            'delivery_date' => Request::Input('minvalue'),
            'pickup_date' => Request::Input('maxvalue'),
            'order_extra' => Request::Input('order_extra'),
            'product_games' => Request::Input('games'),
            'product_id' => $product->id,
            'productname' => $product->productname,
            'price'     => $price,
            'bail'      => $product->bail,
        ];

        Order::create($data);

       $this->sendOrderToAdmin($data);
       $this->sendOrderToUser($data);

     return [
        '.order-userinfo' => $this->renderPartial('forms/states/ordersuccess')
     ];
	}


    public function sendOrderToAdmin($data){
       Mail::send('order.email.admin', $data, function($message) use ($data) {

            $message->to('info@toestelverhuur.nl', 'Thijs Roelofse', $data);
            $message->subject('Nieuwe bestelling');

        });
    }

    public function sendOrderToUser($data){
         Mail::send('order.email.user', $data, function($message) use ($data) {

            $message->to($data['email'], $data['fullname']);
            $message->subject('Uw bestelling is geplaatst');

        });
    }

    public function checkCategory(){

        $slug = $this->param('category');

        if($slug == "consoles"){
            return true;
        } else{
            return false;
        }
    }

    public static $messages = array(
        'policy.required' => 'U dient akkoord te gaan',
        '*.required' => 'Dit veld is verplicht',
        'email.email' => 'Dit emailadres is niet geldig',
        'minvalue.after' => 'Datum is eerder/gelijk aan vandaag',
        'maxvalue.after' => 'Datum is eerder/gelijk aan vandaag ',
        'minvalue.date'   => 'Ongeldige datum',
        'maxvalue.date'   => 'Ongeldige datum',
    );
}



